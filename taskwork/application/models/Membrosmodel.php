<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Membrosmodel extends CI_Model {

    //VIZUALIZAR
	public function vizualizarMembrosGrupo()
	{
		$this->db->where('Grupo_id1',$this->uri->segment(3));
	    return $this->db->get('Membros')->result(); 
	}
	
	
	//CADASTRAR MEMBROS DE GRUPO
    public function cadastrarMembros()
    {
        $dados = array(
            'Usuario_id'           => $this->input->post('Usuario_id'),
			'Grupo_id1'            => $this->input->post('Grupo_id')
        );
		
        return $this->db->insert('Membros', $dados);
    }
    
	//ALTERAR MEMBROS DE GRUPO
    public function alterarMembros()
    {
        $dados = array(
            'Usuario_id'           => $this->input->post('Usuario_id'),
			'Grupo_id1'            => $this->input->post('Grupo_id')
        );
        $id = $this->input->post('id');
        
        $this->db->where('id',$id);
        return $this->db->update('Membros', $dados);    
    }
    
}
