<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Arquivosmodel extends CI_Model {

	//METODOS DE VIZUALIZAR
	public function vizualizarArquivo()
	{
	    $this->db->where('id',$this->uri->segment(3));
	     return $this->db->get('Arquivos')->result();
	}
	public function vizualizarArquivoTarefa()
	{
	    $this->db->where('Tarefas_id',$this->uri->segment(3));
	     return $this->db->get('Arquivos')->result();
	}
    
	//CADASTRAR 
    public function cadastrarArquivo()
    {
        $dados = array(
            'nome'            => $this->input->post('nome'),
            'Tarefas_id'      => $this->input->post('Tarefas_id')
        );
        $this->db->insert('Arquivos', $dados);
        return $this->db->insert_id();
    }
    //ALTERAR
    public function alterarArquivo()
    {
        $dados = array(
            'nome'            => $this->input->post('nome'),
            'Tarefas_id'      => $this->input->post('Tarefas_id')
        );
        $id = $this->input->post('id');
        
        $this->db->where('id',$id);
        return $this->db->update('Arquivos', $dados);    
    }
    
}
