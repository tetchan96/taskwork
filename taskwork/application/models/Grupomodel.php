<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grupomodel extends CI_Model {

	//METODOS DE VIZUALIZAR
	public function vizualizarGrupo()
	{
		$this->db->where('id',$this->uri->segment(3));
	    return $this->db->get('Grupo')->result(); 
	}
	//
	public function vizualizarGrupoUsuario()
	{
		$this->db->where('Usuario_id',$this->uri->segment(3));
		$this->db->join('Grupo G', 'M.Grupo_id1 = G.id'); 
	    return $this->db->get('Membros M')->result(); 	
	}


    //CADASTRAR GRUPO
    public function cadastrarGrupo()
    {
        $dados = array(
            'nome'            => $this->input->post('nome')
        );
        
        $this->db->insert('Grupo', $dados);
        return $this->db->insert_id();
    }
    //ALTERAR GRUPO
    public function alterarGrupo()
    {
        $dados = array(
            'nome'            => $this->input->post('nome')
        );
        $id = $this->input->post('id');
        
        $this->db->where('id',$id);
        return $this->db->update('Grupo', $dados);    
    }
    
}
