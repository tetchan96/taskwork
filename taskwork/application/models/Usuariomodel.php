<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UsuarioModel extends CI_Model {

	//PEGAR TODOS DADOS DO USUARIO
	public function vizualizarDadosUsuario()
	{
	    $this->db->where('id',$this->input->post("id"));
	    return $this->db->get('Usuario')->result();
	}

	//VALIDAR O LOGIN
	public function checaLogin()
	{
	    $this->db->where('login',$this->input->post('login'));
        $this->db->where('senha',sha1($this->input->post('senha')));
	    $r = $this->db->get('Usuario');
        if($r->num_rows() == 1)
        {
            return $r->result();
        }
        else{
            return false;
        }
	}
    //CASTRAR USUARIO
    public function cadastrarUsuario()
    {
        $dados = array(
            'nome'            => $this->input->post('nome'),
            'login'           => $this->input->post('login'),
            'senha'           => sha1($this->input->post('senha')),
            'telefone'        => $this->input->post('telefone')
        );
        
        $this->db->insert('Usuario', $dados);
        return $this->db->insert_id();
    }
    
	//ALTERAR DADOS DO USUARIO
    public function alterarUsuario()
    {
        $dados = array(
            'nome'            => $this->input->post('nome'),
            'login'           => $this->input->post('login'),
            'senha'           => sha1($this->input->post('senha')),
            'login'           => $this->input->post('login'),
            'telefone'        => $this->input->post('telefone'),
            'data_nascimento' => $this->input->post('data_nascimento'),
            'foto'            => $this->input->post('foto')
        );
        $id = $this->input->post('id');
        
        $this->db->where('id',$id);
        return $this->db->update('Usuario', $dados);    
    }
    
}
