<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tarefasmodel extends CI_Model {
	
	//METODOS DE VIZUALIZAR
	public function vizualizarTarefa()
	{
		$this->db->where('id',$this->uri->segment(3));
	    return $this->db->get('Tarefas')->result(); 
	}
	public function vizualizarTarefaGrupo()
	{
		$this->db->where('Grupo_id',$this->uri->segment(3));
	    return $this->db->get('Tarefas')->result(); 
	}
	public function vizualizarTarefaUsuario()
	{
		$this->db->where('Usuario_id',$this->uri->segment(3));
	    return $this->db->get('Tarefas')->result(); 
	}
	public function vizualizarTodasTarefas()
	{
		$this->db->where('M.Usuario_id',$this->uri->segment(3));
		$this->db->join('Grupo G', 'M.Grupo_id1 = G.id'); 
		$this->db->join('Tarefas T', 'T.Grupo_id = G.id'); 
	    return $this->db->get('Membros M')->result(); 
		
	}



	//CADASTRAR
	public function cadastrarTarefa()
    {
        $dados = array(
            'nome'            => $this->input->post('nome'),
            'descricao'       => $this->input->post('descricao'),
            'status'          => $this->input->post('status'),
            'data_inicio'     => $this->input->post('data_inicio'),
            'data_entrega'    => $this->input->post('data_entrega'),
            'Grupo_id' 		  => $this->input->post('Grupo_id'),
            'Usuario_id'      => $this->input->post('Usuario_id')
        );
        
        $this->db->insert('Tarefas', $dados);
        return $this->db->insert_id();
    }
	//ALTERAR
	public function alterarTarefa()
    {
        $dados = array(
            'nome'            => $this->input->post('nome'),
            'descricao'       => $this->input->post('descricao'),
            'status'          => $this->input->post('status'),
            'data_inicio'     => $this->input->post('data_inicio'),
            'data_entrega'    => $this->input->post('data_entrega'),
            'Grupo_id' 		  => $this->input->post('Grupo_id'),
            'Usuario_id'      => $this->input->post('Usuario_id')
        );
        $id = $this->input->post('id');
        
        $this->db->where('id',$id);
        return $this->db->update('Tarefas', $dados);    
    }
	
}
