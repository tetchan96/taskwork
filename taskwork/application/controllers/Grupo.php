<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grupo extends CI_Controller {

	function __construct()
    {
        parent::__construct();

        // Load the necessary stuff...
        $this->load->model('Grupomodel');
    }
	
	public function vizualizarGrupo()
	{
		header("Access-Control-Allow-Origin: *");
		$r = $this->Grupomodel->vizualizarGrupo();
        if(!$r)
        {
           $dados = array('error'=>1);
        }
        else 
        {
            $dados = array('id'=>$r[0]->id,'Nome'=> $r[0]->Nome, 'error'=>0);     
        }
		echo json_encode($dados);  
	}
	public function vizualizarGrupoUsuario()
	{
		header("Access-Control-Allow-Origin: *");
		$r = $this->Grupomodel->vizualizarGrupoUsuario();
        if(!$r)
        {
           $dados = array('error'=>1);
        }
        else 
        {
            foreach($r as $valor)
			{
            	$dados[] = array('id'=>$valor->id,'Nome'=> $valor->Nome, 'error'=>0);     
       		}                   
        }
		echo json_encode($dados); 
	}
	
	
	
	
	//CADASTRAR GRUPO
	public function cadastrarGrupo()
    {
        $id = $this->Grupomodel->cadastrarGrupo();
        if($id != null)
        {
          $dados = array('id'=>$id,'nome'=>$this->input->post('nome'), 'error'=>0);
        }
        else
        {
          $dados = array('error'=>1,'msg'=>'Erro ao tentar realizar o cadastro, tente novamente');     
        }
        echo json_encode($dados);
    }
	//ALTERAR GRUPO
	public function alterarGrupo()
    {
       if($this->Grupomodel->alterarGrupo())
       {
         	$dados = array('error'=>0);    
       }
       else
       {
           $dados = array('error'=>1,'msg'=>'Erro ao tentar alterar, tente novamente');  
       }
	   echo	json_encode($dados);
    }
	
	
	
	
	
	
	
	
	
}
