<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {

	function __construct()
    {
        parent::__construct();

        // Load the necessary stuff...
        $this->load->model('Usuariomodel');
        
    }
	
	public function vizualizarDadosUsuario()
	{
	    header("Access-Control-Allow-Origin: *");
		$r = $this->Usuariomodel->vizualizarDadosUsuario();
        if(!$r)
        {
           $dados = (array('error'=>1));
        }
        else 
        {
            $dados = array('id'=>$r[0]->id,'nome'=> $r[0]->nome, 'login'=> $r[0]->login,'telefone'=> $r[0]->telefone,'data_nascimento'=> $r[0]->data_nascimento, 'foto'=> $r[0]->foto, 'error'=>0);     
            
        }
		echo json_encode($dados);   
	}
	
	//VALIDAR LOGIN
	public function checaUsuario()
	{
		header("Access-Control-Allow-Origin: *");
		$r = $this->Usuariomodel->checaLogin();
        if(!$r)
        {
           $dados = (array('error'=>1));
        }
        else 
        {
            $dados = array('id'=>$r[0]->id,'nome'=> $r[0]->nome, 'error'=>0);        
        }
		echo json_encode($dados);  
	}
    //CADASTRAR USUARIO
    public function cadastrarUsuario()
    {
        $id = $this->Usuariomodel->cadastrarUsuario();
        if($id != null)
        {
          $dados = array('id'=>$id,'nome'=>$this->input->post('nome'), 'error'=>0);
        }
        else
        {
          $dados = array('error'=>1,'msg'=>'Erro ao tentar realizar o cadastro, tente novamente');     
        }
        echo json_encode($dados);
    }
	//ALTERAR DADOS USUARIO
    public function alterarUsuario()
    {
       if($this->Usuariomodel->alterarUsuario())
       {
           $dados = array('error'=>0);  
       }
       else
       {
           $dados = array('error'=>1,'msg'=>'Erro ao tentar alterar, tente novamente');  
       }
	   echo json_encode($dados);
    }








}
