<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Membros extends CI_Controller {

	function __construct()
    {
        parent::__construct();

        // Load the necessary stuff...
        $this->load->model('Membrosmodel');
        
    }
	
	public function vizualizarMembrosGrupo()
	{
		 $r = $this->Membrosmodel->vizualizarMembrosGrupo();
        if(!$r)
        {
            $dados = (array('error'=>1));
        }
        else 
        {  
            foreach($r as $valor)
			{
            	$dados[] = array('Grupo_id'=> $valor->Grupo_id1, 'Usuario_id'=> $valor->Usuario_id, 'error'=>0);     
       		}  
        }
		echo json_encode($dados);
	}
	
	public function cadastrarMembros()
    {
        $r = $this->Membrosmodel->cadastrarMembros();
        if($r)
        {
          $dados = array('Grupo_id1'=>$this->input->post('Grupo_id'),'Usuario_id'=>$this->input->post('Usuario_id'), 'error'=>0);
        }
        else
        {
          $dados = array('error'=>1,'msg'=>'Erro ao tentar realizar o cadastro, tente novamente');     
        }
        echo json_encode($dados);
    }
	
	public function alterarMembros()
    {
       if($this->MembrosModel->alterarMembros())
       {
           
       }
       else
       {
           $dados = array('error'=>1,'msg'=>'Erro ao tentar alterar, tente novamente');  
       }
	   echo json_encode($dados);
    }
	
	
	
	
	
	
	
	
	
}
