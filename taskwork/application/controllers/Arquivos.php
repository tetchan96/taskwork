<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Arquivos extends CI_Controller {

	function __construct()
    {
        parent::__construct();

        // Load the necessary stuff...
        $this->load->model('Arquivosmodel');
        
    }
	
	public function vizualizarArquivoTarefa()
	{
	    $r = $this->Tarefasmodel->vizualizarTarefaGrupo();
        foreach($r as $valor)
		{
            $dados[] = array('id'=>$valor->id,'nome'=> $valor->nome,'Tarefas_id'=> $valor->Tarefas_id, 'error'=>0);     
        }
		echo json_encode($dados);  
	}
	
	public function vizualizarArquivo()
	{
		$r = $this->Arquivosmodel->vizualizarArquivo();
        if(!$r)
        {
            return  json_encode(array('error'=>1));
        }
        else 
        {
            $dados = array('id'=>$r[0]->id,'nome'=> $r[0]->nome,'Tarefas_id'=> $r[0]->Tarefas_id, 'error'=>0);     
            echo json_encode($dados);   
        }
	}
	
	public function cadastrarArquivo()
    {
        $id = $this->Arquivosmodel->cadastrarArquivo();
        if($id != null)
        {
          $dados = array('id'=>$id,'nome'=>$this->input->post('nome'), 'error'=>0);
        }
        else
        {
          $dados = array('error'=>1,'msg'=>'Erro ao tentar realizar o cadastro, tente novamente');     
        }
        json_encode($dados);
    }
	
	public function alterarArquivo()
    {
       if($this->Arquivosmodel->alterarArquivo())
       {
           
       }
       else
       {
           $dados = array('error'=>1,'msg'=>'Erro ao tentar alterar, tente novamente');  
       }
    }
	
	
	
	
	
	
	
	
	
}
