<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tarefas extends CI_Controller {

	function __construct()
    {
        parent::__construct();

        // Load the necessary stuff...
        $this->load->model('Tarefasmodel');
        
    }
	//Dando erro
	public function vizualizarTarefa()
	{
		 $r = $this->Tarefasmodel->vizualizarTarefa();
        if(!$r)
        {
            $dados = (array('error'=>1));
        }
        else 
        {
            $dados = array('id'=>$r[0]->id,'nome'=> $r[0]->nome,'descricao'=> $r[0]->descricao,
				'status'=> $r[0]->status, 'data_inicio'=> $r[0]->data_inicio,'data_entrega'=> $r[0]->data_entrega,
				'Grupo_id'=> $r[0]->Grupo_id, 'Usuario_id'=> $r[0]->Usuario_id, 'error'=>0);     
              
        }
		echo json_encode($dados); 
	}
	
	public function vizualizarTarefaGrupo()
	{
		$r = $this->Tarefasmodel->vizualizarTarefaGrupo();
		if(!$r)
		{
			$dados = (array('error'=>1));
		}
		else
		{
			foreach($r as $valor)
			{
            	$dados[] = array('id'=>$valor->id,'nome'=> $valor->nome,'descricao'=> $valor->descricao,
					'status'=> $valor->status, 'data_inicio'=> $valor->data_inicio,'data_entrega'=> $valor->data_entrega,
					'Grupo_id'=> $valor->Grupo_id, 'Usuario_id'=> $valor->Usuario_id, 'error'=>0);     
       	 	}
		}
        
		echo json_encode($dados);   
       
	}
	
	public function vizualizarTarefaUsuario()
	{
		$r = $this->Tarefasmodel->vizualizarTarefaUsuario();
        foreach($r as $valor)
		{
            $dados[] = array('id'=>$valor->id,'nome'=> $valor->nome,'descricao'=> $valor->descricao,
				'status'=> $valor->status, 'data_inicio'=> $valor->data_inicio,'data_entrega'=> $valor->data_entrega,
				'Grupo_id'=> $valor->Grupo_id, 'Usuario_id'=> $valor->Usuario_id, 'error'=>0);     
        }
		echo json_encode($dados);   
       
	}
	
	public function vizualizarTodasTarefas()
	{
		$r = $this->Tarefasmodel->vizualizarTodasTarefas();
        foreach($r as $valor)
		{
            $dados[] = array('id'=>$valor->id,'nome'=> $valor->nome,'descricao'=> $valor->descricao,
				'status'=> $valor->status, 'data_inicio'=> $valor->data_inicio,'data_entrega'=> $valor->data_entrega,
				'Grupo_id'=> $valor->Grupo_id, 'Usuario_id'=> $valor->Usuario_id, 'error'=>0);     
        }
		echo json_encode($dados); 
	
	}

	
	public function cadastrarTarefa()
    {
        $id = $this->Tarefasmodel->cadastrarTarefa();
        if($id != null)
        {
          $dados = array('id'=>$id,'nome'=>$this->input->post('nome'), 'error'=>0);
        }
        else
        {
          $dados = array('error'=>1,'msg'=>'Erro ao tentar realizar o cadastro, tente novamente');     
        }
        echo json_encode($dados);
    }
	
	public function alterarTarefa()
    {
       if($this->Tarefasmodel->alterarTarefa())
       {
           
       }
       else
       {
           $dados = array('error'=>1,'msg'=>'Erro ao tentar alterar, tente novamente');  
       }
	   
    }
	
	
	
	
	
	
	
	
	
}
